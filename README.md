# What is OctoPrint?

[![OctoPrint](https://octoprint.org/assets/img/logo.png "OctoPrint")](https://octoprint.org/)

[OctoPrint](https://octoprint.org/) provides a web interface for controlling 3D printers, allowing the user to start a print job by sending G-code to a 3D printer connected via USB. OctoPrint monitors the status of the print job, as well as the printer itself, primarily the temperature of the print head (hot end) and the temperature of the bed, if the bed on the printer is heated. OctoPrint can also show the output of a connected webcam in order to monitor the state of the print, and can visualize the G-code in sync with the print job, or asynchronously.

OctoPrint also provides a plugin system, allowing users to extend functionality. There are currently over 150 plugins listed in the official plugin repository. These include advanced timelapse videos that trigger by layer and position the model and print head properly, 3D design collection sites such as MyMiniFactory, STL.garden, integrating OctoPrint with Android apps and Android Wear modules, a Pebble smartwatch,, software integrations with printers such as BigBox3D, MakerGear M3, and Robo 3D printers, and both software and hardware integrations with Prusa i3, Proforge 2S, and the industrial-level GEWO HTP 260 and AON3D printers. It has also been used by Thomas Sanladerer in a PrintrBot as a self-contained and fully mobile printer build.

https://wikipedia.org/wiki/OctoPrint

# How to use this image

## Start an OctoPrint instance
```
docker run -d \
           --name octoprint \
           --privileged \
           -v octoprint:/root/.octoprint \
           -v /var/run/docker.sock:/var/run/docker.sock \
           --device /dev/ttyUSB0:/dev/ttyACM0 \
           -p 80:5000 \
           --restart always \
       davidhamm/octoprint:latest
```

- **`--privileged`**  
  is necessary vor OctoPrint to match Container intern user rights to Host user rights  
- **`-v octoprint:/root/.octoprint`**  
  mounts a volume called 'octoprint' into the container for storing settings outside of the container  
- **`- /var/run/docker.sock:/var/run/docker.sock`**  
  mounts the host docker socket into the octoprint container itself for beeing able to restart and shutdown from within the container  
- **`--device /dev/ttyUSB0:/dev/ttyACM0`**  
  mounts the actual serial port to which the printer is connected into the container. please check your serial port before mounting  
- **`-p 80:5000`**  
  binds the host port '80' to the coinainers internal port '5000'. OctoPrint is listening by default on port '5000' inside the container  
- **`--restart always`**  
  restarts the container always as long as the user doesn't shut's it down

## Common plugins

There is also a version with common plugins preinstalled:

- [Bed Visualizer](https://plugins.octoprint.org/plugins/bedlevelvisualizer/):  
  Displays 3D mesh of bed topography report.  
- [Cancel Objects](https://plugins.octoprint.org/plugins/cancelobject/):  
  Cancel single objects during a print based on gcode comment lines  
- [Cost Estimation](https://plugins.octoprint.org/plugins/costestimation/):  
  Displays the estimated print cost for the loaded model  
- [Filament Manager](https://plugins.octoprint.org/plugins/filamentmanager/):  
  Filament Manager for OctoPrint  
- [Firmware Updater](https://plugins.octoprint.org/plugins/firmwareupdater/):  
  Flash pre-compiled firmware images from OctoPrint  
- [Octolapse](https://plugins.octoprint.org/plugins/octolapse/):  
  Create a stabilized timelapse of your 3D prints. Highly customizable, loads of presets, lots of fun.  
- [Printer Stats](https://plugins.octoprint.org/plugins/stats/):  
  Statistics of your 3D Printer  
- [Themeify](https://plugins.octoprint.org/plugins/themeify/):  
  Beautiful themes for octoprint

in order to start octoprint with this plugins preinstalled, just start a container from **`davidhamm/octoprint:1.3.12-common-plugins`** like so:

<pre><code>docker run -d \
           --name octoprint \
           --privileged \
           -v octoprint:/root/.octoprint \
           -v /var/run/docker.sock:/var/run/docker.sock \
           --device /dev/ttyUSB0:/dev/ttyACM0 \
           -p 80:5000 \
           --restart always \
       <b>davidhamm/octoprint:1.3.12-common-plugins</b>
</code></pre>

## Server commands

This container supports restarting itself from within the OctoPrint UI.  
Go to OctoPrint's **`Settings`**, select **`Server`** from the navigation menu and put **'docker restart octoprint'** into the field **`Restart OctoPrint`**.

## FFMPEG
Ffmpeg comes preinstalled with this container. it is located in **`/usr/bin/ffmpeg`**.  
In order to create timelapses go to OctoPrint's **`Settings`**, select **`Webcam & Timelapse`** and put this path into the field **`Path to FFMPEG`**.

## Webinterface

OctoPrint will be available on this URL:

```
http://localhost
```
